package com.events.eventsproject;

import com.events.eventsproject.events.OrderCompletedEvent;
import org.springframework.context.ApplicationEvent;
import org.springframework.context.ApplicationEventPublisher;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.UUID;

@RestController
@RequestMapping
public class TestController {

    private final ApplicationEventPublisher eventPublisher;

    public TestController(ApplicationEventPublisher eventPublisher) {
        this.eventPublisher = eventPublisher;
    }

    @GetMapping("/buy/{name}")
    public void test(@PathVariable String name) {

        OrderCompletedEvent event = new OrderCompletedEvent(this, name);

        eventPublisher.publishEvent(event);
    }

}
