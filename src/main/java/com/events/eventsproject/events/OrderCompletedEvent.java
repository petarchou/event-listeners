package com.events.eventsproject.events;

import org.springframework.context.ApplicationEvent;

public class OrderCompletedEvent extends ApplicationEvent {

    private String orderId;

    public OrderCompletedEvent(Object source, String orderID) {
        super(source);
        this.orderId = orderID;
    }

    public String getOrderId() {
        return orderId;
    }
}
