package com.events.eventsproject.repositories;

import org.springframework.stereotype.Repository;

import java.util.HashMap;
import java.util.Map;

@Repository
public class ItemRepository {

    private final Map<String, Integer> items;

    public ItemRepository() {
        items = new HashMap<>();
        items.put("cola",10);
        items.put("fanta",20);
        items.put("sprite",1);
    }

    public Map<String, Integer> getItems() {
        return items;
    }
}
