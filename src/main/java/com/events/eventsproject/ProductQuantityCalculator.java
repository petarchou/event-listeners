package com.events.eventsproject;

import com.events.eventsproject.events.OrderCompletedEvent;
import com.events.eventsproject.repositories.ItemRepository;
import org.springframework.context.event.EventListener;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Component;
import org.springframework.web.server.ResponseStatusException;

import java.util.Map;

@Component
public class ProductQuantityCalculator {

    private final ItemRepository itemRepository;

    public ProductQuantityCalculator(ItemRepository itemRepository) {
        this.itemRepository = itemRepository;
    }

    @EventListener(OrderCompletedEvent.class)
    public void calculateQuantity(OrderCompletedEvent event) {
        String itemName = event.getOrderId();
        Map<String,Integer> items = itemRepository.getItems();
        if(items.get(itemName) > 0) {
            items.put(itemName,items.get(itemName)-1);
        }
        else {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND,"insufficient item quantity");
        }
    }
}
