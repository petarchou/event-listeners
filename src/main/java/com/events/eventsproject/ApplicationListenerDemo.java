package com.events.eventsproject;

import com.events.eventsproject.events.OrderCompletedEvent;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.ApplicationEvent;
import org.springframework.context.ApplicationListener;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;
import org.springframework.web.context.support.ServletRequestHandledEvent;

@Component
public class ApplicationListenerDemo {

    private static final Logger LOGGER = LoggerFactory.getLogger(ApplicationListenerDemo.class);

    @EventListener(OrderCompletedEvent.class)
    public void onApplicationEvent(OrderCompletedEvent event) {
        LOGGER.info("I have received the following event: {}", event);
    }
}
